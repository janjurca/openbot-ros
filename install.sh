#!/usr/bin/env bash
set -e
# Orange pi one otg usb enable
# cd /boot/dtb
# sudo cp sun8i-h3-orangepi-one.dtb sun8i-h3-orangepi-one.dtb.old
# sudo dtc -I dtb -O dts -o sun8i-h3-orangepi-one.dts sun8i-h3-orangepi-one.dtb
# sudo sed -i -e 's/dr_mode = "otg";/dr_mode = "host";/g' sun8i-h3-orangepi-one.dts
# sudo dtc -I dts -O dtb -o sun8i-h3-orangepi-one.dtb sun8i-h3-orangepi-one.dts

sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
apt update
apt install -y ros-noetic-ros-base
apt install -y ros-noetic-rosserial

echo source /opt/ros/noetic/setup.bash >> ~/.bashrc
source /opt/ros/noetic/setup.bash


git clone https://gitlab.com/kohoutovice/openbot-ros.git

cd openbot-ros

echo "
[Unit]
Description=openbot

[Service]
Type=forking
ExecStart=/bin/bash -c \"source /opt/ros/noetic/setup.bash; source $(pwd)/catkin_ws/devel/setup.bash; /usr/bin/python3 /opt/ros/noetic/bin/roslaunch openbot base.launch &\"
Restart=on-failure

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/openbot.service


cd catkin_ws
catkin_make


systemctl daemon-reload
systemctl enable openbot.service
systemctl start openbot.service
systemctl status openbot.service
