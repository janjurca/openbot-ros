#include <Arduino.h>
#include <ArduinoHardware.h>
#include <ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Range.h>
#include <ros/time.h>

// Definition of basic pins connections
#define EN_L 10
#define IN1_L 4
#define IN2_L 5
#define EN_R 11
#define IN1_R 6
#define IN2_R 7

//HC-SR04 pins
#define ECHO_PIN 2
#define TRIG_PIN 3

//Global vars
long range_time;
double left_speed = 0;
double right_speed = 0;


//Callback function for cmd_vel message
void messageCMDVEL( const geometry_msgs::Twist& msg){
    //wheel_rad is the wheel radius ,wheel_sep is
    double speed_ang = msg.angular.z;
    double speed_lin = msg.linear.x;

    //basic computation of left and right motors pwm value
    //the magic constants are get from linear and rotation speed on specic values
    //and then used for computation. (the magic values was determined by regression function of measurement)
    left_speed = 485*speed_lin - 48*speed_ang;
    right_speed = 485*speed_lin + 48*speed_ang;

    // Still magic corection according to specific measures
    //if (abs(speed_lin) < 0.46 && abs(speed_ang) < 1) {
    //  left_speed = 0;
    //  right_speed = 0;
    //}
    if (abs(speed_lin) > 0.46) {
      left_speed -= 133;
      right_speed -= 133;
    }
    if (abs(speed_ang) > 1) {
      left_speed += 51;
      right_speed += 51;
    }

    // Just handling bigger values
    if (left_speed > 255) {
      left_speed = 255;
    }
    if (right_speed > 255) {
      right_speed = 255;
    }
}


//Handlles and nodes definitions
ros::NodeHandle nh;
ros::Subscriber<geometry_msgs::Twist> sub("cmd_vel", &messageCMDVEL );

sensor_msgs::Range range_msg;
ros::Publisher pub_range( "ultrasound", &range_msg);

//Just function declarations
void leftMotor(int pwm);
void rightMotor(int pwm);

void setup(){
    pinMode(EN_L, OUTPUT);
    pinMode(EN_R, OUTPUT);
    pinMode(IN1_L, OUTPUT);
    pinMode(IN2_L, OUTPUT);
    pinMode(IN1_R, OUTPUT);
    pinMode(IN2_R, OUTPUT);
    digitalWrite(EN_L, LOW);
    digitalWrite(EN_R, LOW);
    digitalWrite(IN1_L, LOW);
    digitalWrite(IN2_L, LOW);
    digitalWrite(IN1_R, LOW);
    digitalWrite(IN2_R, LOW);

    pinMode(TRIG_PIN, OUTPUT);
    pinMode(ECHO_PIN, INPUT);
    // Range message setting
    range_msg.radiation_type = sensor_msgs::Range::ULTRASOUND;
    range_msg.header.frame_id =  "ultrasound";
    range_msg.field_of_view = 0.1;  // just faking some view angle
    range_msg.max_range = 2.3;
    range_msg.min_range = 0.0;

    nh.initNode();
    nh.subscribe(sub);
    nh.advertise(pub_range);
}


void loop(){
    leftMotor(left_speed);
    rightMotor(right_speed);

    //Range measurement
    if ( millis() >= range_time ){
        int r = 0;
        range_msg.range = getRange() / 100;
        range_msg.header.stamp = nh.now();
        pub_range.publish(&range_msg);
        range_time =  millis() + 50; //not do it every cycle
    }

    //spin node
    nh.spinOnce();
}
void leftMotor(int pwm){
    if (pwm > 0){
        digitalWrite(IN1_L, HIGH);
        digitalWrite(IN2_L, LOW);
    }

    if (pwm < 0){
        digitalWrite(IN1_L, LOW);
        digitalWrite(IN2_L, HIGH);
    }

    if (pwm == 0){
        digitalWrite(IN1_L, LOW);
        digitalWrite(IN2_L, LOW);
    }
    analogWrite(EN_L, abs(pwm));
}


void rightMotor(int pwm){
    if (pwm > 0){
        digitalWrite(IN1_R, LOW);
        digitalWrite(IN2_R, HIGH);
    }

    if (pwm < 0){
        digitalWrite(IN1_R, HIGH);
        digitalWrite(IN2_R, LOW);
    }

    if (pwm == 0){
        digitalWrite(IN1_R, LOW);
        digitalWrite(IN2_R, LOW);
    }
    analogWrite(EN_R, abs(pwm));
}

float getRange(){
    digitalWrite(TRIG_PIN, LOW);
    delayMicroseconds(2);
    digitalWrite(TRIG_PIN, HIGH);
    delayMicroseconds(10);
    digitalWrite(TRIG_PIN, LOW);

    long duration = pulseIn(ECHO_PIN, HIGH);
    // convert the time into a distance
    return duration / 58.31;
}
